# sfucourse

A short script to simplyfy getting course outlines from [Simon Fraser University's very nifty website](https://www.sfu.ca/outlines.html).

## Usage

```bash
sfucourse <year> <fall | spring | summer> <faculty> <course> <section>
```

For example, to see section D100 of CA 122 in the Fall semester of 2024,
```bash
$ sfucourse 2024 fall ca 122 d100
```
...which should return, 
```
Title:	Dance Training and Movement Systems I

Times:	Sep 4 – Dec 3, 2024: Mon, Wed, Fri, 9:30–11:20 a.m.GOLDCORP
	    Sep 4 – Dec 3, 2024: Mon, Wed, Fri, 12:30–2:20 p.m.GOLDCORP
	    Sep 4 – Dec 3, 2024: Tue, Thu, 9:30–11:20 a.m.GOLDCORP

```